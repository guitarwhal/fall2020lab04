package inheritance;

public class BookStore {
	public static void main(String[] args){
		Book[] books = new Book[5];
		books[0] = new Book("Why You Should Leave Your Trash Cans Open", "Three Raccoons in a Trench Coat");
		books[1] = new ElectronicBook("Java for Stressed Semester Three Students", "Daniel Pomerantz", 500);
		books[2] = new Book("Papercuts: A Step-by-Step Guide", "Band-Aid Inc.");
		books[3] = new ElectronicBook("JavaScript for Students Who Really Need to Catch Up in That Class", "Jaya Nilakantan", 450);
		books[4] = new ElectronicBook("\"Have you tried turning it off and on again?\"", "Tired IT Team", 600);
		
		for(int i = 0; i < books.length; i++) {
			System.out.println((i+1) + ". " + books[i]);
		}
	}
}
