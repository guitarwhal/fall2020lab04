package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(3.0, 2.0);
		shapes[1] = new Rectangle(1.5, 7.0);
		shapes[2] = new Circle(5.0);
		shapes[3] = new Circle(3.0);
		shapes[4] = new Rectangle(5.0, 5.0);
		
		for(int i = 0; i < shapes.length; i++) {
			System.out.println((i+1) + ". Area: " + shapes[i].getArea() + "\nPerimeter: " + shapes[i].getPerimeter());
		}
	}
}
