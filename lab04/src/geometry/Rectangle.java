package geometry;

public class Rectangle implements Shape{
	private double length;
	private double width;
	
	public Rectangle(double l, double w){
		this.length = l;
		this.width = w;
	}
	
	public double getLength(){
		return this.length;
	}
	
	public double getWidth(){
		return this.width;
	}
	
	public double getArea(){
		return this.length * this.width;
	}
	
	public double getPerimeter(){
		return (2 * this.length) + (2 * this.width);
	}
}
