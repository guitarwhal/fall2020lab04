package geometry;

public class Circle implements Shape{
	private double radius;
	
	public Circle(double r){
		this.radius = r;
	}
	
	public double getArea() {
		return Math.PI * Math.pow(this.radius, 2);
	}
	
	public double getPerimeter(){
		return 2 * Math.PI * this.radius;
	}
}
